/**
 * Class that contains contact information
 */
export class Contact {
  /**
   * Phone number, with or without country code.
   */
  public phoneNumber: string;

  public firstName: string;

  public lastName: string;
}

/**
 * Loads contacts from the database.
 * @returns all contacts
 */
export function getContacts(): Contact[] {
  throw new Error("Not implemented");
}
